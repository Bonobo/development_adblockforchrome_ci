'use strict';

/* For ESLint: List any global identifiers used in this file below */
/* global chrome, getSettings, translate, FilterListUtil, activateTab,
   CustomFilterListUploadUtil, localizePage */

const backgroundPage = chrome.extension.getBackgroundPage();
const { Filter } = backgroundPage;
const { WhitelistFilter } = backgroundPage;
const { Subscription } = backgroundPage;
const { SpecialSubscription } = backgroundPage;
const { DownloadableSubscription } = backgroundPage;
const { parseFilter } = backgroundPage;
const { filterStorage } = backgroundPage;
const { filterNotifier } = backgroundPage;
const { settingsNotifier } = backgroundPage;
const { channelsNotifier } = backgroundPage;
const { Prefs } = backgroundPage;
const { synchronizer } = backgroundPage;
const { Utils } = backgroundPage;
const { isSelectorFilter } = backgroundPage;
const { isWhitelistFilter } = backgroundPage;
const { isSelectorExcludeFilter } = backgroundPage;
const NotificationStorage = backgroundPage.Notification;
const { License } = backgroundPage;
const { isValidTheme } = backgroundPage;
const { abpPrefPropertyNames } = backgroundPage;
const FIVE_SECONDS = 5000;
const TWENTY_SECONDS = FIVE_SECONDS * 4;

const language = navigator.language.match(/^[a-z]+/i)[0];
let optionalSettings = {};
let delayedSubscriptionSelection = null;
const port = chrome.runtime.connect({ name: 'ui' });
let syncErrorCode = 0;

// Function to check the last known Sync Error Code,
// only allows an event handler to run if there is
// no error to prevent data loss
function checkForSyncError(handler) {
  return function syncError(event) {
    if (syncErrorCode >= 400) {
      return;
    }
    handler(event);
  };
}

function displayVersionNumber() {
  const currentVersion = chrome.runtime.getManifest().version;
  $('#version_number').text(translate('optionsversion', [currentVersion]));
}

function startSubscriptionSelection(title, url) {
  const list = document.getElementById('language_select');
  const noFilterListUtil = typeof FilterListUtil === 'undefined' || FilterListUtil === null;
  const customFilterUtilUndefined = typeof CustomFilterListUploadUtil === 'undefined';

  let noCustomFilterListUploadUtil;
  if (customFilterUtilUndefined) {
    noCustomFilterListUploadUtil = true;
  } else {
    noCustomFilterListUploadUtil = CustomFilterListUploadUtil === null;
  }

  if (!list || noFilterListUtil || noCustomFilterListUploadUtil) {
    activateTab('#filters');
    delayedSubscriptionSelection = [title, url];
    return;
  }
  const translatedMsg = translate('subscribeconfirm', title);
  // eslint-disable-next-line no-alert
  if (window.confirm(translatedMsg)) {
    const existingFilterList = FilterListUtil.checkUrlForExistingFilterList(url);

    if (existingFilterList) {
      CustomFilterListUploadUtil.updateExistingFilterList(existingFilterList);
    } else if (/^https?:\/\/[^<]+$/.test(url)) {
      CustomFilterListUploadUtil.performUpload(url, `url:${url}`, title);
    } else {
      // eslint-disable-next-line no-alert
      alert(translate('failedtofetchfilter'));
    }
    // show the link icon for the new filter list, if the advance setting is set and the
    // show links button has been clicked (not visible)
    if (
      optionalSettings
      && optionalSettings.show_advanced_options
      && $('#btnShowLinks').is(':visible') === false
    ) {
      $('.filter-list-link').fadeIn('slow');
    }
  }
}

port.onMessage.addListener((message) => {
  if (message.type === 'app.respond' && message.action === 'addSubscription') {
    const subscription = message.args[0];
    startSubscriptionSelection(subscription.title, subscription.url);
  }
});

port.postMessage({
  type: 'app.listen',
  filter: ['addSubscription'],
});

window.addEventListener('unload', () => port.disconnect());

function loadOptionalSettings() {
  if (backgroundPage && typeof backgroundPage.getSettings !== 'function') {
    // if the backgroudPage isn't available, wait 50 ms, and reload page
    window.setTimeout(() => {
      window.location.reload();
    }, 50);
  }
  if (backgroundPage && typeof backgroundPage.getSettings === 'function') {
    // Check or uncheck each option.
    optionalSettings = backgroundPage.getSettings();
  }
}

// Update Acceptable Ads UI in the General tab. To be called
// when there is a change in the AA and AA Privacy subscriptions
// Inputs: - checkAA: Bool, true if we must check AA
//         - checkAAprivacy: Bool, true if we must check AA privacy
const updateAcceptableAdsUI = function (checkAA, checkAAprivacy) {
  const $aaInput = $('input#acceptable_ads');
  const $aaPrivacyInput = $('input#acceptable_ads_privacy');
  const $aaPrivacyHelper = $('#aa-privacy-helper');
  const $aaYellowBanner = $('#acceptable_ads_info');

  if (!checkAA && !checkAAprivacy) {
    $aaInput.prop('checked', false);
    $aaPrivacyInput.prop('checked', false);
    $aaYellowBanner.slideDown();
    $aaPrivacyHelper.slideUp();
  } else if (checkAA && checkAAprivacy) {
    $aaInput.removeClass('feature').prop('checked', true).addClass('feature');
    $aaPrivacyInput.prop('checked', true);
    $aaYellowBanner.slideUp();
    if (navigator.doNotTrack) {
      $aaPrivacyHelper.slideUp();
    } else {
      $aaPrivacyHelper.slideDown();
    }
  } else if (checkAA && !checkAAprivacy) {
    $aaInput.prop('checked', true);
    $aaPrivacyInput.prop('checked', false);
    $aaYellowBanner.slideUp();
    $aaPrivacyHelper.slideUp();
  }
};

$(document).ready(() => {
  const onSettingsChanged = function (name, currentValue) {
  };
  settingsNotifier.on('settings.changed', onSettingsChanged);
  window.addEventListener('unload', () => {
    settingsNotifier.off('settings.changed', onSettingsChanged);
  });

  loadOptionalSettings();
  displayVersionNumber();
  localizePage();
});
