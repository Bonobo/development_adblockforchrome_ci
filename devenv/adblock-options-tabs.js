'use strict';

/* For ESLint: List any global identifiers used in this file below */
/* global chrome, License, localizePage, getUILanguage */

function tabIsLocked(tabID) {
  const $tabToActivate = $(`.tablink[href=${tabID}]`);
  const $locked = $tabToActivate.parent('li.locked');
  return !!$locked.length;
}

// Output an array of all tab ids in HTML
function allTabIDs() {
  return $('.tablink').map(function getTabId() {
    return $(this).attr('href');
  }).get();
}

// Active tab cannot be #mab at any point
// if mab tab doesn't exist
// Inputs:
//    - tabID -- string (tab ID to activate)
//    - mabExists -- bool (true if enrolled to MAB)
// Output:
//    - tabID -- string (valid tab ID to activate)
function validateTabID(tabID, mabExists) {
  const defaultTabID = mabExists ? '#mab' : '#general';
  const currentTabID = $('.tablink.active').attr('href');

  if (!tabID) {
    return defaultTabID;
  }
  if (tabID === '#mab' && !mabExists) {
    return defaultTabID;
  }
  if (!allTabIDs().includes(tabID)) {
    return defaultTabID;
  }
  if (currentTabID && tabIsLocked(tabID)) {
    return currentTabID;
  }
  if (tabIsLocked(tabID)) {
    return defaultTabID;
  }
  return tabID;
}

// Show or hide myAdBlock subtabs based on
// which tab is currently active. All subtabs
// links must have a data-parent-tab attribute
// Inputs: $activeTab -- jQuery object
function handleSubTabs($activeTab) {
  const activeTabHref = $activeTab.attr('href');
  const $activeTabNestedUL = $(`[data-parent-tab=${activeTabHref}]`);
  const $activeTabUL = $activeTab.closest('ul');
  const subtabIsActive = $activeTabUL[0].hasAttribute('data-parent-tab');
  const parentTabIsActive = !!$activeTabNestedUL.length;

  // hide all subtabs ul elements
  $('[data-parent-tab]').hide();

  if (subtabIsActive) {
    $activeTabUL.show().fadeTo('slow', 1);
  } else if (parentTabIsActive) {
    $activeTabNestedUL.show().fadeTo('slow', 1);
  }
}


// Load tab panel script in the document when the tab is
// activated for the first time.
// Inputs: $activeTabPanel -- jQuery Object
function loadTabPanelScript($activeTabPanel) {
  const activePanelID = $activeTabPanel.attr('id');
  const scriptToLoad = `adblock-options-${activePanelID}.js`;
  const scriptTag = document.createElement('script');
  const alreadyLoaded = $(`script[src="${scriptToLoad}"]`).length > 0;

  if (alreadyLoaded) {
    return;
  } // dont' load twice the same script

  // Don't use $().append(scriptTag) because CSP blocks eval
  scriptTag.src = scriptToLoad;
  document.body.appendChild(scriptTag);
}

// Display tabs and panel based on the current active tab
// Inputs: $activeTab - active tab jQuery object
function displayActiveTab($activeTab) {
  const $activeTabPanel = $($activeTab.attr('href'));
  handleSubTabs($activeTab);
  loadTabPanelScript($activeTabPanel);
  $activeTabPanel.show();
}

function activateTab(tabHref, mabExists) {
  const tabID = validateTabID(tabHref, mabExists);
  const $activeTab = $(`[href=${tabID}]`);
  const $allTabs = $('.tablink');
  const $allTabPanels = $('.tab');

  $allTabs.removeClass('active');
  $allTabPanels.hide();

  $activeTab.addClass('active');

  $.cookie('active_tab', $activeTab.attr('href'), {
    expires: 10,
  });

  displayActiveTab($activeTab);
}

// Load all HTML templates in respective tab panels
// and translate strings on load completion
function loadTabPanelsHTML() {
  const $tabPanels = $('#tab-content .tab');
  let tabsLoaded = 1; // track the tabs that are loaded
  $.each($tabPanels, (i, panel) => {
    const $panel = $(panel);
    const panelID = $(panel).attr('id');

    const panelHTML = `adblock-options-${panelID}.html`;
    $panel.load(panelHTML, () => {

      localizePage();
      tabsLoaded += 1;
      if (tabsLoaded >= $tabPanels.length) {
        // all tabs have been loaded and localized - call
        // any post processing handlers here.

      }
    });
  });
}

// Get active tab ID from cookie or URL hash and activate tab
// and display the tabs and tabel accordingly
function activateTabOnPageLoad(mabExists) {
  // Set active tab from cookie
  let activeTabID = $.cookie('active_tab');

  // Set active tab from hash (has priority over cookie)
  if (window.location && window.location.hash) {
    [activeTabID] = window.location.hash.split('_');
  }
  activateTab(activeTabID, mabExists);
}

$(document).ready(() => {
  // 1. add the myadblock tab if user is enrolled
  //const myAdBlockTabAdded = addMyAdBlockTab();

  // 2. load all the tab panels templates in respective panel DIVs
  loadTabPanelsHTML();

  // 3. Activate tab on page load with cookie, URL hash or default tabID
  activateTabOnPageLoad(false);

  // 4. Activate tab when clicked
  $('.tablink').click(function tabLinkClicked() {
    const tabID = $(this).attr('href');
    activateTab(tabID, false);
  });
});
